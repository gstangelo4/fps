﻿/*
Greg St. Angelo IV
7.26.2017
*/
using System.Collections.Generic;

public static class Extensions
{
    /// <summary>
    /// Adds an array onto the end of an existing list.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="aList"></param>
    /// <param name="aArray"></param>
    public static void AddArray<T>(this List<T> aList, T[] aArray)
    {
        foreach (T o in aArray)
        {
            aList.Add(o);
        }
    }
}
