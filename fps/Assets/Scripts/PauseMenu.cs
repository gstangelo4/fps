﻿/*
Greg St. Angelo IV
7.26.2017
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour
{
    public void ResetGame()
    {
        SceneManager.LoadScene(0);
        Debug.Log("loaded");
    }
}
