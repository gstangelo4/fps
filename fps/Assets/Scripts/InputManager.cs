﻿/*
Greg St. Angelo IV
7.26.2017
*/
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;

public class InputManager : MonoBehaviour
{
    // enumerations
    public enum E_InputState { Disabled, GameMode, GameAndUi, UIOnly }

    // fields
    private bool m_IsInputAllowed;
    private E_InputState m_CurrentInputState;
    private List<FirstPersonController> m_Players;
    private GameObject m_PauseMenu;

    private void Start()
    {
        m_IsInputAllowed = true;
        m_CurrentInputState = E_InputState.GameMode;

        m_Players = new List<FirstPersonController>();
        m_Players.AddArray(FindObjectsOfType<FirstPersonController>());

        m_PauseMenu = FindObjectOfType<PauseMenu>().gameObject;
        m_PauseMenu.SetActive(false);
    }

    private void Update()
    {
        if (m_IsInputAllowed)
        {
            // set this up as a fsm pls
            switch (m_CurrentInputState)
            {
                case E_InputState.Disabled:
                    Cursor.visible = true;
                    break;
                case E_InputState.GameMode:
                    Cursor.visible = false;
                    SetPlayerPauseState(false);
                    break;
                case E_InputState.GameAndUi:
                    Cursor.visible = true;
                    break;
                case E_InputState.UIOnly:
                    Cursor.visible = true;
                    SetPlayerPauseState(true);
                    break;
            }

            if (Input.GetKeyDown(KeyCode.Escape))
            {
                m_PauseMenu.SetActive(!m_PauseMenu.activeInHierarchy);
                if (m_PauseMenu.activeInHierarchy)
                    ChangeState(E_InputState.UIOnly);
                else
                    ChangeState(E_InputState.GameMode);
            }
        }
    }

    private void SetPlayerPauseState(bool aState)
    {
        foreach (FirstPersonController fpc in m_Players)
        {
            fpc.Paused = aState;
        }
    }

    public void ChangeState(E_InputState aNewState)
    {
        m_CurrentInputState = aNewState;
    }
}
