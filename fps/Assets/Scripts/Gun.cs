﻿/*
Greg St. Angelo IV
7.24.2017
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gun : MonoBehaviour
{
    // properties
    public int Ammo { get; set; }
    public float FireRate { get { return m_FireRate; } set { m_FireRate = value; } }
    public float BulletSpeed { get { return m_BulletForce; } set { m_BulletForce = value; } }

    // fields
    [SerializeField] private GameObject m_Bullet;
    [SerializeField] private GameObject m_LaserPointer;
    [SerializeField] private float m_LaserLength;
    [SerializeField] private float m_BulletForce;
    [SerializeField] private float m_FireRate;
    [SerializeField] private Camera m_Camera;

    private bool m_IsFiring;
    private float m_ElapsedTime;
    private float m_SpawnOffset;
    private LineRenderer m_Laser;

    private void Start()
    {
        // inits
        m_IsFiring = false;
        m_ElapsedTime = 0f;
        m_SpawnOffset = 1f;
        m_Camera = GetComponentInParent<Camera>();
        m_Laser = gameObject.AddComponent<LineRenderer>();
        m_Laser.widthMultiplier = 0.1f;
        m_Laser.alignment = LineAlignment.Local;
    }

    private void Update()
    {
        // input for firing
        if (Input.GetButton("Fire1"))
        {
            Fire();
        }

        // constraints for rate of fire
        if (m_IsFiring)
        {
            m_ElapsedTime += Time.deltaTime;
            if (m_ElapsedTime > FireRate)
            {
                m_IsFiring = false;
                m_ElapsedTime = 0f;
            }
        }
    }

    // used for things that must be rendered after movement has been calculated
    private void LateUpdate()
    {
        m_Laser.SetPosition(0, m_LaserPointer.transform.position);
        m_Laser.SetPosition(1, m_Camera.transform.position + m_Camera.transform.forward * m_LaserLength);
    }

    virtual public void Fire()
    {
        // if the gun isnt firing
        if (!m_IsFiring)
        {
            // run fire logic
            // spawn bullet
            Vector3 _spawnPos = m_Camera.transform.position + (m_Camera.transform.forward * m_SpawnOffset);
            GameObject _bullet = Instantiate(m_Bullet, _spawnPos, m_Camera.transform.rotation);

            // shoot it
            if (_bullet.GetComponent<Rigidbody>() != null)
            {
                _bullet.GetComponent<Rigidbody>().AddForce(_bullet.transform.forward * m_BulletForce);
            }
            else
            {
                _bullet.AddComponent<Rigidbody>().AddForce(_bullet.transform.forward * m_BulletForce);
                _bullet.GetComponent<Rigidbody>().collisionDetectionMode = CollisionDetectionMode.ContinuousDynamic;
            }

            // flag as firing once fired
            m_IsFiring = true;
        }
    }
}
